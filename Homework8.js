//Задание №1
var studentsAndPoints = [
'Алексей Петров', 0,
'Ирина Овчинникова', 60,
'Глеб Стукалов', 30,
'Антон Павлович', 30,
'Виктория Заровская', 30,
'Алексей Левенец', 70,
'Тимур Вамуш', 30,
'Евгений Прочан', 60,
'Александр Малов', 0
];
var students = [];
var commonShow = function () {
    console.log('Студент ' + this.name + ' набрал ' + this.point + ' баллов');
};
for(var i = 0; i < studentsAndPoints.length; i += 2) {
    students.push({
        name: studentsAndPoints[i],
        point: studentsAndPoints[i + 1],
        show: commonShow
    });
}
//students[6].show();

////Задание №2
students.push({
    name: 'Николай Фролов',
    point: 0,
    show: commonShow
}, {
    name: 'Олег Боровой',
    point: 0,
    show: commonShow
});
//console.log(students);

//Задание №3
students.forEach(function (student) {
    if(student.name === 'Ирина Овчинникова' || student.name === 'Александр Малов')
        student.point += 30;
    if(student.name === 'Николай Фролов')
        student.point += 10;
});
//console.log(students);

//Задание №4
//students.forEach(function (student) {
//    if(student.point >= 30)
//        student.show();
//});

//Задание №5
students.forEach(function (student) {
    student['worksAmount'] = student.point / 10;
});
//console.log(students);

//Дополнительное задание №6
students.findByName = function (name) {
    //var success = false;
    //students.forEach(function (student) {
    //    if(student.name === name) {
    //        success = true;
    //        return console.log(student);
    //    }
    //});
    //if(!success)
    //    return console.log(undefined);

    function byName(student) {
        if(name === student.name)
            return student;
    }

    //console.log(students.find(byName));
    return students.find(byName);
};

//students.findByName('Александр Малов');

